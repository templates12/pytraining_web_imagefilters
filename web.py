import sys
import os
import base64
import time

#from io import BytesIO
import cherrypy
#from PIL import Image

import filters


CUR_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_PATH = '{}/static'.format(CUR_DIR)


def get_image_base64(fpath):
    #image = Image.open(fpath)
    #buffered = BytesIO()
    #image.save(buffered, format="JPEG")
    #img_str = base64.b64encode(buffered.getvalue())
    #return img_str
    with open(fpath, 'rb') as f:
        fdata = f.read()
    return base64.b64encode(fdata)


class Root(object):
    INDEX_PATH = os.path.join(CUR_DIR, STATIC_PATH, 'index.html')

    @cherrypy.expose
    def index(self):
        #with open(self.INDEX_PATH) as f:
        #    return f.read()
        return '''
            <!DOCTYPE html >
            <html><head><title>Sample</title></head><body class = "index">
                  <div id = "header"><h1>Image Filters</h1></div>
                  <p>Do it!</p>
                  <form action="filter" method = "post">
                     <table summary = "">
                        <tr>
                           <th><label for = "fin">Input file</label></th>
                           <td><input type = "file" id = "fin" name = "fin" /></td>
                        </tr>
                        <tr>
                           <td></td>
                           <td><input type = "submit" name = "show" value = "show" /></td>
                        </tr>   
                  </form>
                  <div id = "footer"><hr></div></body></html>
        '''

    @cherrypy.expose
    #@cherrypy.tools.json_in()
    #@cherrypy.tools.json_out()
    def filter(self, **vars):
        #cherrypy.response.headers['Content-Type'] = 'image/jpg'
        #x, y = cherrypy.request.json
        #return {"x":x, "y":y}
        print("filter: {}".format(vars))
        inputFile = vars.get('fin')
        fpath = '{}/{}'.format(CUR_DIR, inputFile)
        idata = get_image_base64(fpath).decode("utf-8")
        return '''
        <!DOCTYPE html >
        <html>
           <head>
              <title>Sample</title>
           </head>
            
           <body class = "index">
              <div id = "header"><h1>Image Filters</h1></div>
              <p>Do it!</p>
              
              <form action="apply" method = "post">
                 <table>
                    <tr>
                       <th><label for = "finimg">Image</label></th>
                       <td><img src="data:image/jpeg;base64, {}" height = 640 width = 1200 id="finimg" name="finimg"/></td>
                    </tr>

                    <tr>
                       <td></td>
                       <td><input type = "hidden id="fin" name= "fin" value="{}"/></td>
                    </tr>

                    <tr>
                       <td><label for = "fout">Output file</label></td>
                       <td><input type = "text" id="fout" name= "fout" value="{}"/></td>
                    </tr>

                    <tr>
                       <td><label for="bright">bright (between 0 and 3. step=0.1. light > 1, dark < 1 ):</label></td>
                       <!--<td><input type="range" id="bright" name="bright" min="0" max="5" value="1" step="0.1"></td>-->
                       <td><input type="number" id="bright" name="bright" min="0" max="5" value="1" step="0.1"></td>
                    </tr>

                    <tr>
                       <td><label for="contrast">contrast (between 0 and 10. step=0.1. strong > 1):</label></td>
                       <td><input type="range" id="contrast" name="contrast" min="0" max="10" value="1" step="0.1"></td>
                    </tr>

                    <tr>
                       <td><label for="negative">negative:</label></td>
                       <td><input type="checkbox" id="negative" name="negative"></td>
                    </tr>         

                    <tr><td></td><td><input type = "submit" value = "submit" /></td></tr>

                 </table>
                 
              </form>
              <div id = "footer"><hr></div>
           </body>
        </html>'''.format(idata, fpath, os.path.dirname(os.path.abspath(fpath)))

    @cherrypy.expose
    def apply(self, **vars):
        print("apply: {}".format(vars))
        inputFile = vars.get('fin')
        outputFile = vars.get('fout')

        if vars.get('bright'):
            bright = float(vars.get('bright'))
        else:
            bright = 0

        if vars.get('contrast'):
            contrast = float(vars.get('contrast'))
        else:
            contrast = 0

        if vars.get('negative') == 'on':
            negative = 1
        else:
            negative = 0

        filters.Filters.runFilters(bright, contrast, negative, inputFile, outputFile)
        time.sleep(1)
        idata = get_image_base64(inputFile).decode("utf-8")
        idata2 = get_image_base64(outputFile).decode("utf-8")

        return '''
        <!DOCTYPE html >
        <html>
           <head>
              <title>Sample</title>
           </head>
            
           <body class = "index">
              <div id = "header"><h1>Image Filters</h1></div>
              <p>Do it!</p>
              <div>BEFORE:<img src="data:image/jpeg;base64, {}" height = 640 width = 1200 /></div>
              <div>AFTER:<img src="data:image/jpeg;base64, {}" height = 640 width = 1200 /></div>
              <div id = "footer"><hr></div>
           </body>
        </html>'''.format(idata, idata2)


class API(object):
    @cherrypy.expose
    def ver(self, **vars):
        return {"version": 1.0}


def runWeb():
    root = Root()
    root.api = API()
    autoreload = False
    conf = {
        '/': {
            'tools.staticdir.on': True,
            'tools.staticdir.root': CUR_DIR,
            'tools.staticdir.dir': STATIC_PATH,
        },
    }

    cherrypy.config.update({'server.socket_port': 8080,
                            'log.screen': True,
                            'engine.autoreload.on': autoreload,
                            'log.access_file': '',
                            'log.error_file': ''})

    cherrypy.server.socket_host = '0.0.0.0'
    cherrypy.server.socket_port = 8080
    cherrypy.server.thread_pool = 5
    cherrypy.quickstart(root, '/', conf)
