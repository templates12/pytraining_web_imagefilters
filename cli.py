import argparse
import sys
import os

from PIL import Image

import web
import filters


""" python3 cli.py -b 1.4 -n 0 -c 1.3 -i img.jpg -o out.jpg """
""" python3 cli.py -w 1 """

CUR_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_PATH = '{}/static'.format(CUR_DIR)


class CLI(object):
    @staticmethod
    def get_params(sys_args):
        doParser = argparse.ArgumentParser(add_help=False)
        doParser.add_argument('-w', dest='web', required=False, type=int, help='run web app')
        doParser.add_argument('-b', dest='bright', required=False, help='brightness. X < 1 - dark; X > 1 - light')
        doParser.add_argument('-n', dest='negative', required=False, help='negative. 1 - negative')
        doParser.add_argument('-c', dest='contrast', required=False, help='contrast. X > 1 - strong contrast. 1,2,3,...')
        doParser.add_argument('-i', dest='inputFile', required=False, help='input file')
        doParser.add_argument('-o', dest='outputFile', required=False, help='output file')

        args = doParser.parse_args()
        if args is None:
            args = sys_args[1:]
        return args


def main(sys_args=None):

    args = CLI.get_params(sys_args)
    print(args)

    bright = 0
    if args.bright:
        bright = float(args.bright)
    
    contrast = 0
    if args.contrast:
        contrast = float(args.contrast)

    negative = 0
    if args.negative:
        negative = int(args.negative)

    print(bright, contrast, negative)


    if(args.web):
        web.runWeb()
    else:
        filters.Filters.runFilters(bright, contrast, negative, args.inputFile, args.outputFile)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
