import argparse
import sys
import os

from PIL import Image


class Filters(object):
    @staticmethod
    def negative(source_name, result_name):
        source = Image.open(source_name)
        result = Image.new('RGB', source.size)
        for x in range(source.size[0]):
            for y in range(source.size[1]):
                r, g, b = source.getpixel((x, y))
                result.putpixel((x, y), (255 - r, 255 - g, 255 - b))
        result.save(result_name, "JPEG")

    @staticmethod
    def bright(source_name, result_name, brightness):
        source = Image.open(source_name)
        result = Image.new('RGB', source.size)
        for x in range(source.size[0]):
            for y in range(source.size[1]):
                r, g, b = source.getpixel((x, y))

                red = int(r * brightness)
                red = min(255, max(0, red))

                green = int(g * brightness)
                green = min(255, max(0, green))

                blue = int(b * brightness)
                blue = min(255, max(0, blue))

                result.putpixel((x, y), (red, green, blue))
        result.save(result_name, "JPEG")

    @staticmethod
    def contrast(source_name, result_name, coefficient):
        source = Image.open(source_name)
        result = Image.new('RGB', source.size)

        avg = 0
        for x in range(source.width):
            for y in range(source.height):
                r, g, b = source.getpixel((x, y))
                avg += r * 0.299 + g * 0.587 + b * 0.114
        avg /= source.width * source.height

        palette = []
        for i in range(256):
            temp = int(avg + coefficient * (i - avg))
            if temp < 0:
                temp = 0
            elif temp > 255:
                temp = 255
            palette.append(temp)

        for x in range(source.width):
            for y in range(source.height):
                r, g, b = source.getpixel((x, y))
                result.putpixel((x, y), (palette[r], palette[g], palette[b]))

        result.save(result_name, "JPEG")

    @staticmethod
    def contast2(source_name, result_name, brightness):
        img = Image.open(source_name)
        pixels = img.load()
        result = Image.new('RGB', img.size)
        pixels2 = img.load()
        for x in range(img.width):
            for y in range(img.height):
                r, g, b = pixels[x, y]
                pixels[x, y] = g, b, r
        img.save('result.jpg')

    @staticmethod
    def runFilters(bright, contrast, negative, inputFile, outputFile):
        if (bright and contrast and negative):  # 1 1 1 - 7
            Filters.negative(inputFile, outputFile)
            Filters.bright(outputFile, outputFile, bright)
            Filters.contrast(outputFile, outputFile, contrast)
            print("case7")
        elif (bright and contrast and not negative):  # 1 1 0 - 6
            Filters.bright(inputFile, outputFile, bright)
            Filters.contrast(outputFile, outputFile, contrast)
            print("case6")
        elif (bright and not contrast and negative):  # 1 0 1 - 5
            Filters.negative(inputFile, outputFile)
            Filters.bright(outputFile, outputFile, bright)
            print("case5")
        elif (bright and not contrast and not negative):  # 1 0 0 - 4
            Filters.bright(inputFile, outputFile, bright)
            print("case4")
        elif (not bright and contrast and negative):  # 0 1 1 - 3
            Filters.negative(inputFile, outputFile)
            Filters.contrast(outputFile, outputFile, contrast)
            print("case3")
        elif (not bright and contrast and not negative):  # 0 1 0 - 2
            Filters.contrast(inputFile, outputFile, contrast)
            print("case2")
        elif (not bright and not contrast and negative):  # 0 0 1 - 1
            Filters.negative(inputFile, outputFile)
            print("case1")
