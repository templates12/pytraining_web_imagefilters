Python Example for Image filters
1. bright
2. contrast
3. negative
4. cli
5. web application
 
install requirements
- pip3 install -r requirements.txt

run app
- cli: python3 cli.py -b 1.4 -n 0 -c 1.3 -i img.jpg -o out.jpg
- web: python3 cli.py -w 1
